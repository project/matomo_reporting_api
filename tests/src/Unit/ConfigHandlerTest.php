<?php

declare(strict_types=1);

namespace Drupal\Tests\matomo_reporting_api\Unit;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Tests\UnitTestCase;
use Drupal\matomo_reporting_api\ConfigHandler;
use Drupal\matomo_reporting_api\ConfigHandlerInterface;

/**
 * Unit tests for the ConfigHandler.
 *
 * @group matomo_reporting_api
 * @coversDefaultClass \Drupal\matomo_reporting_api\ConfigHandler
 */
class ConfigHandlerTest extends UnitTestCase {

  /**
   * The mocked config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $configFactory;

  /**
   * The mocked module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $moduleHandler;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->configFactory = $this->createMock(ConfigFactoryInterface::class);
    $this->moduleHandler = $this->createMock(ModuleHandlerInterface::class);
  }

  /**
   * @covers ::getAuthenticationToken
   */
  public function testGetAuthenticationToken(): void {
    $expected_token = $this->randomMachineName(32);

    $this->expectConfigStorageRetrieval('matomo_reporting_api.settings', 'token_auth', $expected_token);

    $config_handler = $this->getConfigHandler();
    $this->assertEquals($expected_token, $config_handler->getAuthenticationToken());
  }

  /**
   * @covers       ::getSiteID
   * @dataProvider getSiteIdProvider
   */
  public function testGetSiteId(bool $matomo_exists, bool $inherit_matomo_settings, string $expected_site_id): void {
    // We expect that a single call should be made to query whether the Matomo
    // module is enabled.
    $this->expectCheckIfMatomoSettingsAreInherited($matomo_exists, $inherit_matomo_settings);

    if ($matomo_exists) {
      // If the Matomo module exists then a second call will be made to either
      // the Matomo or Matomo Reporting API config storage to retrieve the
      // requested value.
      $requested_value_config = $this->getMockBuilder('Drupal\Core\Config\ImmutableConfig')
        ->disableOriginalConstructor()
        ->getMock();
      $requested_value_config
        ->expects($this->once())
        ->method('get')
        ->with('site_id')
        ->will($this->returnValue($expected_site_id));

      // Only retrieve the settings from the Matomo module if the module is
      // enabled and we are inheriting its settings.
      $config_name = $matomo_exists && $inherit_matomo_settings ? 'matomo.settings' : 'matomo_reporting_api.settings';
      $this->configFactory
        ->expects($this->at(1))
        ->method('get')
        ->with($config_name)
        ->will($this->returnValue($requested_value_config));
    }
    else {
      // If the Matomo module doesn't exist, there should just be a single call
      // that retrieves the config from the Matomo Reporting API.
      $this->expectConfigStorageRetrieval('matomo_reporting_api.settings', 'site_id', $expected_site_id);
    }

    $config_handler = $this->getConfigHandler();
    $this->assertEquals($expected_site_id, $config_handler->getSiteID());
  }

  /**
   * Data provider for ::testGetSiteId().
   *
   * @return array[]
   *   An array of test cases, each test case an indexed array with:
   *   - A boolean representing whether the Matomo module is enabled.
   *   - A boolean representing whether the Matomo settings are inherited.
   *   - A string representing the expected site ID.
   */
  public function getSiteIdProvider(): array {
    return [
      [
      // The Matomo module is not enabled.
        FALSE,
      // And the settings from it are not inherited.
        FALSE,
      // So we expect the value configured in Matomo Reporting API to be
      // returned.
        '1',
      ],
      [
      // The Matomo module is not enabled.
        FALSE,
      // And the settings from it are inherited (but this should be ignored).
        TRUE,
      // So we expect the value configured in Matomo Reporting API to be
      // returned.
        '1',
      ],
      [
      // The Matomo module is enabled.
        TRUE,
      // But the settings from it are not inherited.
        FALSE,
      // So we expect the value configured in Matomo Reporting API to be
      // returned.
        '1',
      ],
      [
      // The Matomo module is enabled.
        TRUE,
      // And its settings are inherited.
        TRUE,
      // So we expect its value to be returned.
        '2',
      ],
    ];
  }

  /**
   * @covers       ::getUrl
   * @dataProvider getUrlProvider
   */
  public function testGetUrl(bool $matomo_exists, bool $inherit_matomo_settings, string $https_url, string $http_url, ?string $expected_url): void {
    $this->expectCheckIfMatomoSettingsAreInherited($matomo_exists, $inherit_matomo_settings);

    // If the Matomo module exists then a call to the config factory was already
    // made to check if we are inheriting config, so we set the counter to 1.
    $at = $matomo_exists ? 1 : 0;

    // We expect that the service will first check if the HTTPS URL is set.
    $requested_value_config = $this->getMockBuilder('Drupal\Core\Config\ImmutableConfig')
      ->disableOriginalConstructor()
      ->getMock();
    $requested_value_config
      ->expects($this->once())
      ->method('get')
      ->with('url_https')
      ->will($this->returnValue($https_url));

    // Only retrieve the settings from the Matomo module if the module is
    // enabled and we are inheriting its settings.
    $config_name = $matomo_exists && $inherit_matomo_settings ? 'matomo.settings' : 'matomo_reporting_api.settings';
    $this->configFactory
      ->expects($this->at($at++))
      ->method('get')
      ->with($config_name)
      ->will($this->returnValue($requested_value_config));

    // If the HTTPS URL is empty then another call is made to retrieve the HTTP
    // URL.
    if (empty($https_url)) {
      $requested_value_config = $this->getMockBuilder('Drupal\Core\Config\ImmutableConfig')
        ->disableOriginalConstructor()
        ->getMock();
      $requested_value_config
        ->expects($this->once())
        ->method('get')
        ->with('url_http')
        ->will($this->returnValue($http_url));

      // Only retrieve the settings from the Matomo module if the module is
      // enabled and we are inheriting its settings.
      $config_name = $matomo_exists && $inherit_matomo_settings ? 'matomo.settings' : 'matomo_reporting_api.settings';
      $this->configFactory
        ->expects($this->at($at++))
        ->method('get')
        ->with($config_name)
        ->will($this->returnValue($requested_value_config));
    }

    $config_handler = $this->getConfigHandler();
    $this->assertEquals($expected_url, $config_handler->getUrl());
  }

  /**
   * Data provider for ::testGetUrl().
   *
   * @return array[]
   *   An array of test cases, each test case an indexed array with:
   *   - A boolean representing whether the Matomo module is enabled.
   *   - A boolean representing whether the Matomo settings are inherited.
   *   - A string representing the HTTPS URL that is configured.
   *   - A string representing the HTTP URL that is configured.
   *   - A string representing the expected URL.
   */
  public function getUrlProvider(): array {
    return [
      [
      // The Matomo module is not enabled.
        FALSE,
      // And the settings from it are not inherited.
        FALSE,
      // The HTTPS URL is not configured.
        '',
      // Neither is the HTTP URL.
        '',
      // So we expect that the function will return NULL.
        NULL,
      ],
      [
        TRUE,
        FALSE,
        '',
        '',
        NULL,
      ],
      [
        FALSE,
        TRUE,
        '',
        '',
        NULL,
      ],
      [
        TRUE,
        TRUE,
        '',
        '',
        NULL,
      ],
      [
        FALSE,
        FALSE,
        'https://matomo.mydomain.com',
        '',
        'https://matomo.mydomain.com',
      ],
      [
        TRUE,
        FALSE,
        'https://matomo.mydomain.com',
        '',
        'https://matomo.mydomain.com',
      ],
      [
        FALSE,
        TRUE,
        'https://matomo.mydomain.com',
        '',
        'https://matomo.mydomain.com',
      ],
      [
        TRUE,
        TRUE,
        'https://matomo.mydomain.com',
        '',
        'https://matomo.mydomain.com',
      ],
      [
        FALSE,
        FALSE,
        '',
        'http://matomo.mydomain.com',
        'http://matomo.mydomain.com',
      ],
      [
        TRUE,
        FALSE,
        '',
        'http://matomo.mydomain.com',
        'http://matomo.mydomain.com',
      ],
      [
        FALSE,
        TRUE,
        '',
        'http://matomo.mydomain.com',
        'http://matomo.mydomain.com',
      ],
      [
        TRUE,
        TRUE,
        '',
        'http://matomo.mydomain.com',
        'http://matomo.mydomain.com',
      ],
      [
        FALSE,
        FALSE,
        'https://matomo.mydomain.com',
        'http://matomo.mydomain.com',
        'https://matomo.mydomain.com',
      ],
      [
        TRUE,
        FALSE,
        'https://matomo.mydomain.com',
        'http://matomo.mydomain.com',
        'https://matomo.mydomain.com',
      ],
      [
        FALSE,
        TRUE,
        'https://matomo.mydomain.com',
        'http://matomo.mydomain.com',
        'https://matomo.mydomain.com',
      ],
      [
        TRUE,
        TRUE,
        'https://matomo.mydomain.com',
        'http://matomo.mydomain.com',
        'https://matomo.mydomain.com',
      ],
    ];
  }

  /**
   * Returns the config handler service.
   *
   * This is the system under test.
   *
   * @return \Drupal\matomo_reporting_api\ConfigHandlerInterface
   *   The config handler, instantiated with mocked dependencies.
   */
  protected function getConfigHandler(): ConfigHandlerInterface {
    return new ConfigHandler($this->configFactory, $this->moduleHandler);
  }

  /**
   * Sets up the expected sequence for retrieving a config value from storage.
   *
   * @param string $name
   *   The name of the config object for which a value will be retrieved.
   * @param string $key
   *   The key of the config value to be retrieved.
   * @param mixed $value
   *   The expected value that should be returned by the config storage.
   */
  protected function expectConfigStorageRetrieval(string $name, string $key, $value): void {
    $immutable_config_object = $this->getMockBuilder('Drupal\Core\Config\ImmutableConfig')
      ->disableOriginalConstructor()
      ->getMock();
    $immutable_config_object
      ->expects($this->once())
      ->method('get')
      ->with($key)
      ->will($this->returnValue($value));
    $this->configFactory
      ->expects($this->once())
      ->method('get')
      ->with($name)
      ->will($this->returnValue($immutable_config_object));
  }

  /**
   * Sets up the expected API calls for checking whether we inherit settings.
   *
   * @param bool $matomo_exists
   *   Whether the Matomo module is enabled.
   * @param bool $inherit_matomo_settings
   *   Whether the site builder has chosen to inherit the settings from the
   *   Matomo module.
   */
  protected function expectCheckIfMatomoSettingsAreInherited(bool $matomo_exists, bool $inherit_matomo_settings) {
    // We expect that a single call should be made to query whether the Matomo
    // module is enabled.
    $this->moduleHandler
      ->expects($this->once())
      ->method('moduleExists')
      ->will($this->returnValue($matomo_exists));

    if ($matomo_exists) {
      // If the module exists, then we expect that the config that indicates if
      // the Matomo settings are inherited is requested.
      $inherit_settings_config = $this->getMockBuilder('Drupal\Core\Config\ImmutableConfig')
        ->disableOriginalConstructor()
        ->getMock();
      $inherit_settings_config
        ->expects($this->once())
        ->method('get')
        ->with('inherit_matomo_settings')
        ->will($this->returnValue($inherit_matomo_settings));
      $this->configFactory
        ->expects($this->at(0))
        ->method('get')
        ->with('matomo_reporting_api.settings')
        ->will($this->returnValue($inherit_settings_config));
    }
  }

}
