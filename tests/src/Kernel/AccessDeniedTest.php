<?php

declare(strict_types=1);

namespace Drupal\Tests\matomo_reporting_api\Kernel;

use Drupal\Core\Url;
use Drupal\KernelTests\KernelTestBase;

/**
 * Tests that no errors occur when the Matomo server denies access.
 *
 * @group matomo_reporting_api
 */
class AccessDeniedTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'matomo',
    'matomo_reporting_api',
    'matomo_reporting_api_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig('matomo');

    // Connect to our mocked Matomo server that always returns a 403.
    // Strip the 'matomo.php' filename, this is added automatically.
    $url = Url::fromRoute('matomo_reporting_api_test.access_denied')->setAbsolute()->toString();
    $url = substr($url, 0, strrpos($url, '/') + 1);
    $this->config('matomo.settings')->set('url_http', $url)->save();
  }

  /**
   * Tests that FALSE is returned as API version when Matomo denies access.
   */
  public function testApiVersionWhenMatomoDeniesAccess(): void {
    $result = matomo_reporting_api_get_api_version();
    $this->assertFalse($result);
  }

}
