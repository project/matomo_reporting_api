<?php

/**
 * @file
 * Post update functions for the Matomo Reporting API module.
 */

declare(strict_types=1);

/**
 * Copy the configuration from the Matomo module.
 *
 * This ensures the Matomo Reporting API module can work independently
 * if the Matomo module is disabled. It migrates the `site_id`, `url_http`,
 * and `url_https` settings to the new configuration.
 */
function matomo_reporting_api_post_update_copy_matomo_config() {
  // Previous versions of the module required the Matomo module for its server
  // configuration. We now allow the module to be used standalone, so ensure to
  // copy over the config so that we can still connect to the server if the
  // Matomo module is disabled in the future.
  if (\Drupal::moduleHandler()->moduleExists('matomo')) {
    $config_factory = \Drupal::configFactory();
    $matomo_config = $config_factory->get('matomo.settings');
    $matomo_reporting_api_config = $config_factory->getEditable('matomo_reporting_api.settings');
    $matomo_reporting_api_config->set('inherit_matomo_settings', TRUE);

    foreach (['site_id', 'url_http', 'url_https'] as $key) {
      $matomo_reporting_api_config->set($key, $matomo_config->get($key));
    }

    $matomo_reporting_api_config->save();
  }
}
