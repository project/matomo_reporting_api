# Matomo Reporting API example module

This module contains an example of how to retrieve data from the Matomo
Reporting API. It provides a block that shows the most popular pages,
according to the number of visits.
