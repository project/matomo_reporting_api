<?php

/**
 * @file
 * Install, uninstall, update and requirements hooks for Matomo Reporting API.
 */

declare(strict_types=1);

use Drupal\Core\Config\Entity\ConfigEntityUpdater;
use Drupal\user\RoleInterface;

/**
 * Implements hook_requirements().
 */
function matomo_reporting_api_requirements($phase) {
  $requirements = [];

  if ($phase == 'runtime') {
    $matomo_version = matomo_reporting_api_get_api_version();

    /** @var \Drupal\matomo_reporting_api\ConfigHandlerInterface $config_handler */
    $config_handler = \Drupal::service('matomo_reporting_api.config_handler');

    // Throw an error when the authentication token is not set.
    if (empty($config_handler->getAuthenticationToken())) {
      $requirements['matomo_reporting_api_missing_token'] = [
        'title' => t('Matomo reporting API'),
        'value' => t('Authentication token not set.'),
        'description' => t('Please <a href=":configuration_form">provide an authentication token</a> to enable communication with the Matomo server.', [
          ':configuration_form' => \Drupal::service('url_generator')->generateFromRoute('matomo_reporting_api.settings_form'),
        ]),
        'severity' => REQUIREMENT_ERROR,
      ];
      return $requirements;
    }
    // Throw an error when the Matomo server cannot be reached.
    elseif ($matomo_version === FALSE) {
      $requirements['matomo_reporting_api_server_unreachable'] = [
        'title' => t('Matomo reporting API'),
        'value' => t('Matomo server cannot be reached.'),
        'description' => t('Please <a href=":matomo_reporting_api_configuration_form">verify</a> the authentication token as well as the Matomo site ID and URL.', [
          ':matomo_reporting_api_configuration_form' => \Drupal::service('url_generator')->generateFromRoute('matomo_reporting_api.settings_form'),
        ]),
        'severity' => REQUIREMENT_ERROR,
      ];
      return $requirements;
    }

    // Warn the user if they are using insecure HTTP transport.
    $url = $config_handler->getUrl();
    if (empty($url) || parse_url($url)['scheme'] !== 'https') {
      $requirements['matomo_reporting_api_https'] = [
        'title' => t('Matomo reporting API'),
        'value' => t('Matomo user credentials are not encrypted and visible on the network.'),
        'description' => t('The user authentication token is transmitted without encryption and is vulnerable to being discovered on the network. Attackers can use this token to take over the Matomo server. It is <strong>highly recommended</strong> to provide an encrypted HTTPS URL for Matomo on production environments.'),
        'severity' => REQUIREMENT_WARNING,
      ];
      $value = t('Matomo @version', ['@version' => $matomo_version]);
    }
    else {
      $value = t('Matomo @version. Communication with the Matomo server is handled using a secure connection.', ['@version' => $matomo_version]);
    }
    $requirements['matomo_reporting_api'] = [
      'title' => t('Matomo reporting API'),
      'value' => $value,
    ];
  }

  return $requirements;
}

/**
 * Implements hook_install().
 */
function matomo_reporting_api_install() {
  if (\Drupal::moduleHandler()->moduleExists('piwik_reporting_api')) {
    // If 'piwik_reporting_api' module was previously enabled migrate its
    // configuration and disable the module.
    $config_factory = \Drupal::configFactory();
    $config = $config_factory->get('piwik_reporting_api.settings');
    $data = $config->getRawData();
    $config_factory->getEditable('matomo_reporting_api.settings')
      ->setData($data)
      ->save();
    \Drupal::service('module_installer')->uninstall(['piwik_reporting_api']);
  }
}

/**
 * Drop dependency on `matomo` module.
 */
function matomo_reporting_api_update_9201(?array $sandbox = NULL): void {
  \Drupal::classResolver(ConfigEntityUpdater::class)->update($sandbox, 'user_role', function (RoleInterface $role): bool {
    if ($role->hasPermission('administer matomo')) {
      $role->grantPermission('administer matomo reporting api');
      return TRUE;
    }
    return FALSE;
  });
}
