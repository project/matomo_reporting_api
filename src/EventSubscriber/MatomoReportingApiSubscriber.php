<?php

declare(strict_types=1);

namespace Drupal\matomo_reporting_api\EventSubscriber;

use Drupal\Core\Config\ConfigCrudEvent;
use Drupal\Core\Config\ConfigEvents;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Matomo Reporting API event subscriber.
 */
class MatomoReportingApiSubscriber implements EventSubscriberInterface {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a MatomoReportingApiSubscriber event subscriber.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * Synchronises the configuration from the Matomo module.
   *
   * We are sourcing the connection info for the Matomo server from the Matomo
   * module. Whenever this configuration changes, sync the info to our own
   * config storage so that we can still connect to the server in case the
   * Matomo module is disabled in the future.
   *
   * @param \Drupal\Core\Config\ConfigCrudEvent $event
   *   The event that fires on config CRUD operations.
   */
  public function onConfigSave(ConfigCrudEvent $event) {
    $matomo_config = $event->getConfig();
    if ($matomo_config->getName() === 'matomo.settings') {
      $matomo_reporting_api_config = $this->configFactory->getEditable('matomo_reporting_api.settings');
      if ($matomo_reporting_api_config->get('inherit_matomo_settings')) {
        foreach (['site_id', 'url_http', 'url_https'] as $key) {
          $matomo_reporting_api_config->set($key, $matomo_config->get($key));
        }
        $matomo_reporting_api_config->save();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      ConfigEvents::SAVE => ['onConfigSave'],
    ];
  }

}
