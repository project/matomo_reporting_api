<?php

declare(strict_types=1);

namespace Drupal\matomo_reporting_api\Form;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configuration form for the Matomo Reporting API module.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Constructs a new SettingsForm for the Matomo Reporting API module.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   * @param TypedConfigManagerInterface $typed_config_manager
   *   The typed config manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ModuleHandlerInterface $module_handler, ClientInterface $http_client, TypedConfigManagerInterface $typed_config_manager) {
    parent::__construct($config_factory, $typed_config_manager);
    $this->moduleHandler = $module_handler;
    $this->httpClient = $http_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('module_handler'),
      $container->get('http_client'),
      $container->get('config.typed'),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['matomo_reporting_api.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'matomo_reporting_api_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('matomo_reporting_api.settings');
    $form['token_auth'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Authentication token'),
      '#description' => $this->t('The Matomo user authentication token. Get it from the Matomo web interface at Administration > Platform > API.'),
      '#maxlength' => 32,
      '#size' => 32,
      '#default_value' => $config->get('token_auth'),
    ];

    // We need the Matomo site ID and URL, which are settings that are normally
    // configured in the Matomo module. If this module is not enabled, the user
    // can set them here.
    $matomo_exists = $this->moduleHandler->moduleExists('matomo');
    $form['inherit_matomo_settings'] = [
      '#type' => 'checkbox',
      '#title' => $matomo_exists ? $this->t(
          'Use server connection settings from the <a href=":url">Matomo configuration form</a>.', [
            ':url' => Url::fromRoute('matomo.admin_settings_form')->toString(),
          ]
      ) : '',
      '#default_value' => $config->get('inherit_matomo_settings'),
      '#access' => $matomo_exists,
    ];

    $form['settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Server connection settings'),
      '#open' => TRUE,
      '#states' => [
        'invisible' => [
          ':input[name="inherit_matomo_settings"]' => ['checked' => TRUE],
        ],
        'disabled' => [
          ':input[name="inherit_matomo_settings"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['settings']['site_id'] = [
      '#default_value' => $config->get('site_id'),
      '#description' => $this->t('The user account number is unique to the websites domain. Click the <strong>Settings</strong> link in your Matomo account, then the <strong>Websites</strong> tab and enter the appropriate site <strong>ID</strong> into this field.'),
      '#maxlength' => 20,
      '#required' => TRUE,
      '#size' => 15,
      '#title' => $this->t('Matomo site ID'),
      '#type' => 'number',
      '#min' => 0,
      '#step' => 1,
    ];
    $url_https = $config->get('url_https');
    if (!empty($url_https) && parse_url($url_https, PHP_URL_SCHEME) !== 'https') {
      $this->messenger()->addWarning(
            $this->t(
                'Your Matomo URL %url is not secure! Use HTTPS to protect your visitors data.',
                ['%url' => $url_https]
            )
        );
    }
    $form['settings']['url_https'] = [
      '#default_value' => $url_https,
      '#placeholder' => 'https://analytics.example.com/',
      '#description' => $this->t('The URL of your Matomo application.'),
      '#maxlength' => 255,
      '#size' => 80,
      '#title' => $this->t('Matomo URL'),
      '#type' => 'url',
    ];
    $form['settings']['url_http'] = [
      '#default_value' => $config->get('url_http'),
      '#description' => $this->t('(deprecated) The HTTP (insecure) URL of your Matomo application. Do not use unless you really need to because your visitors data might not be safe!'),
      '#maxlength' => 255,
      '#size' => 80,
      '#title' => $this->t('Matomo HTTP (insecure) URL'),
      '#type' => 'url',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    // Validate the site ID.
    $form_state->setValue('site_id', trim($form_state->getValue('site_id')));
    if (!preg_match('/^\d{1,}$/', $form_state->getValue('site_id'))) {
      $form_state->setErrorByName('site_id', $this->t('A valid Matomo site ID is an integer only.'));
    }

    // Check that the HTTPS URL is secure.
    $url_https = $form_state->getValue('matomo_url_https');
    if (!empty($url_https) && parse_url($url_https, PHP_URL_SCHEME) !== 'https') {
      $form_state->setErrorByName(
            'matomo_url_https', $this->t(
                'Your Matomo URL %url is not secure! Use HTTPS to protect your visitors data.',
                ['%url' => $url_https]
            )
        );
    }

    // Validate the URLs.
    foreach (['url_https', 'url_http'] as $key) {
      $url = $form_state->getValue($key);
      if (!empty($url)) {
        if (substr($url, -strlen('/')) !== '/') {
          $url .= '/';
          $form_state->setValueForElement($form['settings'][$key], $url);
        }
        $url = $url . 'matomo.php';

        try {
          $result = $this->httpClient->request('GET', $url);
          if ($result->getStatusCode() != 200) {
            $form_state->setErrorByName(
                  $key, $this->t(
                      'The validation of "@url" failed with error "@error" (HTTP code @code).', [
                        '@url' => UrlHelper::filterBadProtocol($url),
                        '@error' => $result->getReasonPhrase(),
                        '@code' => $result->getStatusCode(),
                      ]
                  )
              );
          }
        }
        catch (RequestException $exception) {
          $form_state->setErrorByName(
                $key, $this->t(
                    'The validation of "@url" failed with an exception "@error" (HTTP code @code).', [
                      '@url' => UrlHelper::filterBadProtocol($url),
                      '@error' => $exception->getMessage(),
                      '@code' => $exception->getCode(),
                    ]
                )
            );
        }
      }
    }

    // Either the HTTP or HTTPS URL must be entered.
    if (empty($form_state->getValue('url_http')) && empty($form_state->getValue('url_https'))) {
      $form_state->setErrorByName('url_https', $this->t('The Maromo URL is required.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $config = $this->config('matomo_reporting_api.settings');

    // Don't change the setting to inherit the server connection from Matomo if
    // the module is not enabled, since in this case the setting is hidden.
    if ($this->moduleHandler->moduleExists('matomo')) {
      $config->set('inherit_matomo_settings', $form_state->getValue('inherit_matomo_settings'));
    }

    foreach (['token_auth', 'site_id', 'url_http', 'url_https'] as $key) {
      $config->set($key, $form_state->getValue($key));
    }

    $config->save();
  }

}
