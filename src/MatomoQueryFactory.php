<?php

declare(strict_types=1);

namespace Drupal\matomo_reporting_api;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\matomo_reporting_api\Exception\MissingMatomoServerUrlException;
use GuzzleHttp\Client;
use Matomo\ReportingApi\QueryFactory;
use Matomo\ReportingApi\QueryInterface;

/**
 * Factory for Matomo query objects.
 *
 * This wraps the query factory from the Matomo Reporting API PHP library.
 *
 * @see \Matomo\ReportingApi\QueryFactory
 */
class MatomoQueryFactory implements MatomoQueryFactoryInterface {

  /**
   * The Guzzle HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * The config handler.
   *
   * @var \Drupal\matomo_reporting_api\ConfigHandlerInterface
   */
  protected $configHandler;

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * The query factory from the Matomo Reporting API PHP library.
   *
   * @var \Matomo\ReportingApi\QueryFactoryInterface
   */
  protected $queryFactory;

  /**
   * Constructs a new MatomoQueryFactory.
   *
   * @param \GuzzleHttp\Client $httpClient
   *   The Guzzle HTTP client.
   * @param \Drupal\matomo_reporting_api\ConfigHandlerInterface $configHandler
   *   The config handler.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   *   The logger factory.
   */
  public function __construct(Client $httpClient, ConfigHandlerInterface $configHandler, LoggerChannelFactoryInterface $loggerFactory) {
    $this->httpClient = $httpClient;
    $this->configHandler = $configHandler;
    $this->loggerFactory = $loggerFactory;
  }

  /**
   * {@inheritdoc}
   */
  public function getQuery($method) {
    $factory = $this->getQueryFactory();
    $query = $factory->getQuery($method);
    $this->setDefaultParameters($query);
    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function getQueryFactory() {
    if (empty($this->queryFactory)) {
      $this->queryFactory = $this->createFactoryInstance();
    }

    return $this->queryFactory;
  }

  /**
   * Retrieves default parameters from config and applies them to the query.
   *
   * @param \Matomo\ReportingApi\QueryInterface $query
   *   The query to which to apply the default parameters.
   */
  protected function setDefaultParameters(QueryInterface $query) {
    // The user authentication token.
    if ($token_auth = $this->configHandler->getAuthenticationToken()) {
      $query->setParameter('token_auth', $token_auth);
    }

    // The site ID.
    if ($site_id = $this->configHandler->getSiteID()) {
      $query->setParameter('idSite', $site_id);
    }
  }

  /**
   * Generates and returns a new instance of the factory, with defaults applied.
   *
   * @return \Matomo\ReportingApi\QueryFactoryInterface
   *   The query factory.
   *
   * @throws \Drupal\matomo_reporting_api\Exception\MissingMatomoServerUrlException
   *   Thrown when the Matomo server URL is not configured.
   */
  protected function createFactoryInstance() {
    $url = $this->configHandler->getUrl();

    // Log an error if the Matomo server URL is not configured.
    if (empty($url)) {
      $this->loggerFactory->get('matomo_reporting_api')->error('Matomo cannot be queried. The URL of the Matomo server is not configured.');
      throw new MissingMatomoServerUrlException();
    }

    // Log a warning if the communication with the Matomo server is insecure.
    if (parse_url($url)['scheme'] !== 'https') {
      $this->loggerFactory->get('matomo_reporting_api')->warning('The communication with the Matomo server is insecure. Make sure to use HTTPS for the Matomo server URL so that the user credentials are safely encrypted and cannot be abused by potential attackers.');
    }

    return new QueryFactory($url, $this->httpClient);
  }

}
