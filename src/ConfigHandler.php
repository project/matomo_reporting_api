<?php

declare(strict_types=1);

namespace Drupal\matomo_reporting_api;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Handlers configuration for the Matomo Reporting API module.
 *
 * This module is usually used as an extension of the Matomo module but can also
 * be used standalone. In case the Matomo module is enabled we are reusing their
 * config, so that a site builder doesn't need to input the same settings twice.
 */
class ConfigHandler implements ConfigHandlerInterface {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Whether or not to use the settings from the Matomo module.
   *
   * @var bool|null
   */
  protected $inheritMatomoSettings;

  /**
   * Constructs a ConfigHandler object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ModuleHandlerInterface $module_handler) {
    $this->configFactory = $config_factory;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public function getAuthenticationToken(): ?string {
    $matomo_reporting_api_config = $this->configFactory->get('matomo_reporting_api.settings');
    return $matomo_reporting_api_config->get('token_auth');
  }

  /**
   * {@inheritdoc}
   */
  public function getSiteId(): ?string {
    return $this->getSharedData('site_id');
  }

  /**
   * {@inheritdoc}
   */
  public function getUrl(): ?string {
    $https_url = $this->getSharedData('url_https');
    return !empty($https_url) ? $https_url : $this->getSharedData('url_http');
  }

  /**
   * Returns the configuration for the given config key.
   *
   * This will return configuration from the Matomo module if it is enabled,
   * with a fallback to the configuration from Matomo Reporting API if the
   * module is used standalone.
   *
   * @param string $key
   *   The config key for which to return the configured value.
   *
   * @return string|null
   *   The configuration value, or NULL if this has not been configured yet.
   */
  protected function getSharedData(string $key): ?string {
    $name = $this->matomoSettingsInherited() ? 'matomo.settings' : 'matomo_reporting_api.settings';
    return $this->configFactory->get($name)->get($key);
  }

  /**
   * Checks whether we are inheriting the settings from the Matomo module.
   *
   * @return bool
   *   TRUE if the Matomo module is enabled and the site builder has chosen to
   *   inherit its settings.
   */
  protected function matomoSettingsInherited(): bool {
    if (!isset($this->inheritMatomoSettings)) {
      if ($this->moduleHandler->moduleExists('matomo')) {
        $this->inheritMatomoSettings = $this->configFactory->get('matomo_reporting_api.settings')->get('inherit_matomo_settings');
      }
      else {
        $this->inheritMatomoSettings = FALSE;
      }
    }
    return $this->inheritMatomoSettings;
  }

}
