<?php

declare(strict_types=1);

namespace Drupal\matomo_reporting_api;

/**
 * Handlers configuration for the Matomo Reporting API module.
 *
 * This module is usually used as an extension of the Matomo module but can also
 * be used standalone. In case the Matomo module is enabled we are reusing their
 * config, so that a site builder doesn't need to input the same settings twice.
 */
interface ConfigHandlerInterface {

  /**
   * Returns the authentication token.
   *
   * @return string|null
   *   The authentication token, or NULL if no authentication token has yet been
   *   configured.
   */
  public function getAuthenticationToken(): ?string;

  /**
   * Returns the site ID.
   *
   * Note that in the Matomo API the site ID is stored as an integer, but we are
   * returning this as a string, to match the way this data is stored in the
   * parent Matomo module.
   *
   * @return string|null
   *   The site ID, or NULL if no site ID has yet been configured.
   */
  public function getSiteId(): ?string;

  /**
   * Returns the Matomo server URL.
   *
   * @return string|null
   *   The secure HTTPS URL if configured. Falls back to the insecure HTTP, or
   *   NULL if none of the URLs have been configured.
   */
  public function getUrl(): ?string;

}
